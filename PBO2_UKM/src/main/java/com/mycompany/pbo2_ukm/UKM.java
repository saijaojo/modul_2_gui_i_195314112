/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_ukm;

/**
 *
 * @author asus
 */
public class UKM {
    private String  namaUnit;
    private Mahasiswa  ketua;
    private Mahasiswa  sekertaris;
    private Penduduk [] anggota;

    public UKM() {
    }

    public UKM(String namaUnit) {
        this.namaUnit = namaUnit;
    }
    void setNamaunit (String namaUnit){
        this.namaUnit =namaUnit;
    }
    String getnamaUnit (){
        return namaUnit;
    }
    void setKetua (Mahasiswa  dataKetua){
        this.ketua = dataKetua;
    }
    Mahasiswa  gettKetua (){
        return ketua;
    }
    void setSekertaris (Mahasiswa  dataSekertaris){
        this.sekertaris =dataSekertaris;
    }
    Mahasiswa  getSekertaris (){
        return sekertaris;
    }
    void setAnggota (Penduduk [] dataAnggota){
        this.anggota = dataAnggota;
    }
    Penduduk [] getAnggota (){
        return  anggota;
    }    
}
